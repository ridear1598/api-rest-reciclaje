const ErrorResponse = require("../helper/errorResponse");
const Reciclador = require("../models/Reciclador");

exports.getReciclador = async(req, res, next) => {
    try {
        const recicladorlista = await Reciclador.find();
        res.status(200);
        res.json(recicladorlista);
    } catch (err) {
        next(
            new ErrorResponse("No se pudo procesar el request" + err.message, 400)
        );
    }
};

exports.getRecicladorById = async(req, res, next) => {
    try {
        const reciclador = await Reciclador.findById(req.params.id);
        if (!reciclador) {
            return next(new ErrorResponse("El Reciclador no existe", 404));
        }
        res.json({ data: reciclador });
    } catch (err) {
        next(
            new ErrorResponse("Error al mostrar informacion: " + req.params.id, 404)
        );
    }
};

exports.addReciclador = async(req, res, next) => {
    try {
        let reci =req.body;
        const reciclador = await Reciclador.create(reci);
        res.status(200);
        res.json({
            status: 200,
            data: reciclador,
        });
    } catch (err) {
        next(
            new ErrorResponse("No se pudo procesar el request" + err.message, 400)
        );
    }
};

exports.deleteReciclador = async(req, res, next) => {
    try {
        const reciclador = await Reciclador.findByIdAndDelete(req.params.id);
        if (!reciclador) {
            return next(new ErrorResponse("El recilador no existe" + err.message, 400));
        }
        res.status(200);
        res.json({
            status: 200,
            data: reciclador,
        });
    } catch (err) {
        next(
            new ErrorResponse("No se pudo procesar el request" + err.message, 400)
        );
    }
};

exports.updateReciclador = async(req, res, next) => {
    try {
        const reciclador = await Reciclador.findByIdAndUpdate(req.params.id, req.body);
        res.status(200);
        res.json({
            status: 200,
            data: reciclador,
        });
    } catch (err) {
        next(
            new ErrorResponse("No se pudo procesar el request" + err.message, 400)
        );
    }
};