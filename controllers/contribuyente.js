const Contribuyente = require("../models/Contribuyente");
const ErrorResponse = require("../helper/errorResponse");

exports.crearContribuyente = async(req, res, next) => {
    try {
        const contribuyenteData = await Contribuyente.create(req.body);
        res.status(200).json({
            status: 200,
            data: contribuyenteData,
        });
    } catch (err) {
        res.status(400).json({
            status: 400,
            msg: err,
        });
    }
};

exports.getContribuyente = async(req, res, next) => {
    try {
        const contribuyenteLista = await Contribuyente.find();
        res.status(200).json(contribuyenteLista);
    } catch (err) {
        res.status(400).json({
            status: 400,
            msg: err,
        });
    }
};

exports.getContribuyenteById = async(req, res, next) => {
    try {
        const contribuyente = await Contribuyente.findById(req.params.id);
        console.log(contribuyente)
        if (!contribuyente) {
            return next(new ErrorResponse("El Contribuyente no existe", 404));
        }
        res.status(200).json(contribuyente);
    } catch (err) {
        next(
            new ErrorResponse("Error al mostrar informacion : " + req.params.id, 404)
        ); //Ejecuta middelware

        // res.status(400).json({ status: 400, msg: err });
    }
};

exports.updateContribuyente = async(req, res, next) => {
    try {
        const contribuyente = await Contribuyente.findByIdAndUpdate(req.params.id, req.body);

        if (!contribuyente) {
            return res.status(400).json({
                status: 400,
                msg: "el contribuyente no existe",
            });
        }
        res.status(200).json({
            status: 200,
            data: contribuyente,
        });
    } catch (err) {
        res.status(400).json({
            status: 400,
            msg: err,
        });
    }
};

exports.deleteContribuyente = async(req, res, next) => {
    try {
        const contribuyente = await Contribuyente.findByIdAndDelete(req.params.id);

        if (!contribuyente) {
            return res.json({
                status: 400,
                msg: "el contribuyente no existe",
            });
        }
        res.status(200).json({
            status: 200,
            msg: "contribuyente eliminado",
        });
    } catch (err) {
        res.status(400).json({
            status: 400,
            msg: err,
        });
    }
};