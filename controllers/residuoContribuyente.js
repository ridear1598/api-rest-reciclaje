const ErrorResponse = require("../helper/errorResponse");
const Residuo = require("../models/ResiduoContribuyente");

exports.getResiduos = async(req, res, next) => {
    try {
        const residuolista = await Residuo.find();
        res.status(200);
        res.json(residuolista);
    } catch (err) {
        next(
            new ErrorResponse("No se pudo procesar el request" + err.message, 400)
        );
    }
};

exports.getResiduoById = async(req, res, next) => {
    try {
        const residuoUnico = await Residuo.findById(req.params.id);
        if (!residuoUnico) {
            return next(new ErrorResponse("El residuo no existe", 404));
        }
        res.json({ data: residuoUnico });
    } catch (err) {
        next(
            new ErrorResponse("Error al mostrar informacion: " + req.params.id, 404)
        );
    }
};

exports.createResiduo = async(req, res, next) => {
    try {
        const residuoUnico = await Residuo.create(req.body);
        res.status(200);
        res.json({
            status: 200,
            data: residuoUnico,
        });
    } catch (err) {
        next(
            new ErrorResponse("No se pudo procesar el request" + err.message, 400)
        );
    }
};

exports.updateResiduo = async(req, res, next) => {
    try {
        const residuoUnico = await Residuo.findByIdAndUpdate(req.params.id, req.body);
        res.status(200);
        res.json({
            status: 200,
            data: residuoUnico,
        });
    } catch (err) {
        next(
            new ErrorResponse("No se pudo procesar el request" + err.message, 400)
        );
    }
};

exports.deleteResiduo = async(req, res, next) => {
    try {
        const residuoUnico = await Residuo.findByIdAndDelete(req.params.id);
        if (!residuoUnico) {
            return next(new ErrorResponse("El residuo no existe" + err.message, 400));
        }
        res.status(200);
        res.json({
            status: 200,
            data: residuoUnico,
        });
    } catch (err) {
        next(
            new ErrorResponse("No se pudo procesar el request" + err.message, 400)
        );
    }
};