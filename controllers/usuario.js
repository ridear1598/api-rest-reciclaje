const ErrorResponse = require("../helper/errorResponse");
const Usuario = require("../models/Usuario");



exports.login = async(req, res, next) => {
    try {
        const { email, password } = req.body;
        if (!email || !password) {
          return res.json({
               status:0,
               msg:"Ingrese su email y contraseña"
           })
        }
        const usuarioBD = await Usuario.findOne({ email: email }).select('+password');
        if (!usuarioBD) {
          return  res.json({
                status:0,
                msg:"El usuario no existe en la base de datos"
            })
        }
        const valorLogin = await usuarioBD.validarPassword(password);
        if (!valorLogin) {
            return res.json({
                status:0,
                msg:"Las credenciales son incorrectas"
            })
        }

        const token = usuarioBD.crearJsonWebToken()

        //res.status(200);
        res.json({
            status: 1,
            id: usuarioBD._id,
            nombre: usuarioBD.nombre,
            apellidos: usuarioBD.apellidos,
            username: usuarioBD.username,
            email: usuarioBD.email,
            token
        });
    } catch (err) {
        next(
            new ErrorResponse("No se pudo procesar el request" + err.message, 400)
        );
    }
}

exports.createUsuario = async(req, res, next) => {
    try {
        const { nombre, apellidos, username, email, password } = req.body

        const UsuarioUnico = await Usuario.create({
            nombre,
            apellidos,
            username,
            email,
            password
        });
        const token = UsuarioUnico.crearJsonWebToken();

        res.status(200);
        res.json({
            status: 200,
            id: UsuarioUnico._id,
            nombre,
            apellidos,
            username,
            email,
            token
        });
    } catch (err) {
        next(
            new ErrorResponse("No se pudo procesar el request" + err.message, 400)
        );
    }
};

exports.getUsuarios = async(req, res, next) => {
    try {
        const Usuariolista = await Usuario.find();
        res.status(200);
        res.json(Usuariolista);
    } catch (err) {
        next(
            new ErrorResponse("No se pudo procesar el request" + err.message, 400)
        );
    }
};





exports.getUsuarioById = async(req, res, next) => {
    try {
        const UsuarioUnico = await Usuario.findById(req.params.id);
        if (!UsuarioUnico) {
            return next(new ErrorResponse("El Usuario no existe", 404));
        }
        res.json({ data: UsuarioUnico });
    } catch (err) {
        next(
            new ErrorResponse("Error al mostrar informacion: " + req.params.id, 404)
        );
    }
};


exports.updateUsuario = async(req, res, next) => {
    try {
        const UsuarioUnico = await Usuario.findByIdAndUpdate(req.params.id, req.body);
        res.status(200);
        res.json({
            status: 200,
            data: UsuarioUnico,
        });
    } catch (err) {
        next(
            new ErrorResponse("No se pudo procesar el request" + err.message, 400)
        );
    }
};

exports.deleteUsuario = async(req, res, next) => {
    try {
        const UsuarioUnico = await Usuario.findByIdAndDelete(req.params.id);
        if (!UsuarioUnico) {
            return next(new ErrorResponse("El Usuario no existe" + err.message, 400));
        }
        res.status(200);
        res.json({
            status: 200,
            data: UsuarioUnico,
        });
    } catch (err) {
        next(
            new ErrorResponse("No se pudo procesar el request" + err.message, 400)
        );
    }
};