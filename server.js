const dotenv = require("dotenv");
const express = require("express");
const cors = require("cors");
const morgan = require("morgan");
const connectDatabase = require("./config/db");

dotenv.config({ path: "./config/config.env" });
connectDatabase();

const residuoContribuyente = require("./routes/residuoContribuyente");
const contribuyente = require("./routes/contribuyente");
const reciclador = require("./routes/reciclador");
const usuario = require("./routes/usuario");

const errorHandler = require("./middleware/error");

const app = express();
app.use(express.json());
app.use(cors());

if (process.env.NODE_ENV == "development") {
    app.use(morgan("dev"));
}

app.use("/api/residuoContribuyente", residuoContribuyente);
app.use("/api/contribuyente", contribuyente);
app.use("/api/reciclador", reciclador);
app.use("/usuario", usuario)
app.use(errorHandler);

const PORT = process.env.PORT || 5000;

const server = app.listen(
    PORT,
    console.log("el servidor se ejecuta en ", process.env.NODE_ENV)
);

//Intersepatar errores de conexion
process.on("unhandledRejection", (err, promise) => {
    console.log("Errores", err.message);
    server.close(() => process.exit(1));
});