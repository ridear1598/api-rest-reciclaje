const express = require("express");
const ruta = express.Router();

const controllerContribuyente = require("../controllers/contribuyente");

ruta.route("/").post(controllerContribuyente.crearContribuyente);
ruta.route("/").get(controllerContribuyente.getContribuyente);
ruta.route("/:id").get(controllerContribuyente.getContribuyenteById);
ruta.route("/:id").put(controllerContribuyente.updateContribuyente);
ruta.route("/:id").delete(controllerContribuyente.deleteContribuyente);

module.exports = ruta;