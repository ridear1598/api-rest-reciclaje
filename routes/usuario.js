const express = require("express");
const ruta = express.Router();

const controllerUsuario = require("../controllers/usuario");

ruta.route("/create").post(controllerUsuario.createUsuario);
ruta.route("/").get(controllerUsuario.getUsuarios);
ruta.route("/login").post(controllerUsuario.login);


ruta.route("/:id").get(controllerUsuario.getUsuarioById);
ruta.route("/:id").put(controllerUsuario.updateUsuario);
ruta.route("/:id").delete(controllerUsuario.deleteUsuario);

module.exports = ruta;