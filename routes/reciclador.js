const express = require("express");
const ruta = express.Router();

const controllerReciclador = require("../controllers/reciclador");

ruta.route("/").post(controllerReciclador.addReciclador);
ruta.route("/").get(controllerReciclador.getReciclador);
ruta.route("/:id").get(controllerReciclador.getRecicladorById);
ruta.route("/:id").put(controllerReciclador.updateReciclador);
ruta.route("/:id").delete(controllerReciclador.deleteReciclador);

module.exports = ruta;