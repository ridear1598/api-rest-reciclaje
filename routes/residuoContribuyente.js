const express = require("express");
const ruta = express.Router();

const controllerResiduoC = require("../controllers/residuoContribuyente");

ruta.route("/").get(controllerResiduoC.getResiduos);
ruta.route("/").post(controllerResiduoC.createResiduo);
ruta.route("/:id").get(controllerResiduoC.getResiduoById);
ruta.route("/:id").put(controllerResiduoC.updateResiduo);
ruta.route("/:id").delete(controllerResiduoC.deleteResiduo);


module.exports = ruta;