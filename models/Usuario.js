    const mongoose = require("mongoose");
    const bcrypt = require("bcrypt");
    const jwt = require("jsonwebtoken")

    const UsuarioSchema = new mongoose.Schema({
        nombre: {
            type: String,
            require: [true, 'Por favor ingrese un nombre']
        },
        apellidos: {
            type: String,
            require: [true, 'Por favor ingrese el apallido']
        },
        username: {
            type: String,
            require: [true, 'Por favor ingrese un Username']
        },
        email: {
            type: String,
            require: [true, 'Por favor ingrese su email'],
            unique: true
                // match: [
                //     /^([\w-.]+)@(([([0-9]{1,3}.){3}[0-9]{1,3}])|(([\w-]+.)+)([a-zA-Z]{2,4}))$/,
                //     'Ingrese un email valido'
                // ]
        },
        password: {
            type: String,
            require: [true, 'Por favor ingrese un password'],
            minlength: 6,
            select: false,
        }
    });

    UsuarioSchema.pre('save', async function(next) {
        const salt = await bcrypt.genSalt(10);
        this.password = await bcrypt.hash(this.password, salt);

    });


    UsuarioSchema.methods.crearJsonWebToken = function() {
        return jwt.sign({ username: this.username }, process.env.JWT_SECRET_WORD, {
            expiresIn: process.env.JWT_EXPIRED,
        })
    }

    UsuarioSchema.methods.validarPassword = async function(passwordUser) {
        return await bcrypt.compare(passwordUser, this.password)

    }
    module.exports = mongoose.model("Usuario", UsuarioSchema);