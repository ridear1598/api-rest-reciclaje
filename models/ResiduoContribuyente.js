const mongoose = require("mongoose");

const ResiduoContribuyenteSchema = new mongoose.Schema({
    cantidad: String,
    tipo: String,
    FechaRecoleccion: Date,
    contribuyente: { id: String, nombres: String },
});

module.exports = mongoose.model("residuoContribuyente", ResiduoContribuyenteSchema);