const mongoose = require("mongoose");

const RecicladorSchema = new mongoose.Schema({
    nombre: String,
    apellidoPaterno: String,
    apellidoMaterno: String,
    DNI: String,
    edad: String
});

module.exports = mongoose.model("Reciclador", RecicladorSchema);