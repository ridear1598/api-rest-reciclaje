const mongoose = require("mongoose");

const ContribuyenteSchema = new mongoose.Schema({
    nombre: String,
    apellido: String,
    edad: String,
    dni: String,
    direccion: String,
    distrito: String,
    coordenadas: String,
});

module.exports = mongoose.model("Aportante", ContribuyenteSchema);